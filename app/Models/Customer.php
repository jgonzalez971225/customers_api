<?php

namespace App\Models;

use App\Models\Traits\ActivityLog;
use App\Models\Traits\SearchPattern;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{

    use HasFactory , SoftDeletes , SearchPattern , ActivityLog;

    const STATUS_ACTIVE = 'A';

    protected $fillable = [
        'name',
        'last_name',
        'email',
        'dni',
        'address',
        'status',
        'commune_id',
        'region_id'
    ];

    protected $hidden = [
        'updated_at',
        'status'
    ];

    public $searchPattern = ['email','dni'];
    public $statusActive  = 'A';

    public function commune()
    {
        return $this->belongsTo(Commune::class,'commune_id');
    }

    public function region()
    {
        return $this->belongsTo(Region::class,'region_id');
    }

}
