<?php

namespace App\Models;

use App\Models\Traits\SearchPattern;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Commune extends Model
{
    use HasFactory , SoftDeletes , SearchPattern;

    protected $fillable      = ['description','region_id','status'];
    protected $searchPattern = ['description'];
    protected $statusActive  = 'A';

    public function region()
    {
        return $this->belongsTo(Region::class,'region_id');
    }

    public function isBelongs($regionId)
    {
        return $this->region_id === $regionId;
    }
}
