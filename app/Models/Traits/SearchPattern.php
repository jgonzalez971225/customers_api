<?php namespace App\Models\Traits;

trait SearchPattern
{

    public function scopePattern($query , $pattern)
    {
        if (!$pattern){
            return;
        }
        foreach($this->searchPattern as $fieldSearch)
        {
            $query->orWhere($fieldSearch,'LIKE','%'.$pattern."%");
        }
    }

    public function scopeActives($query)
    {
        $query->where('status',$this->statusActive);
    }

}
