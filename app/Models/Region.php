<?php

namespace App\Models;

use App\Models\Traits\SearchPattern;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Region extends Model
{
    use HasFactory, SearchPattern;

    protected $fillable      = ['description','status'];
    protected $searchPattern = ['description'];
    protected $statusActive  = 'A';

    public function communes()
    {
        return $this->hasMany(Commune::class,'region_id');
    }

}
