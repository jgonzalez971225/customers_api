<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return array_filter($this->availableAttributes());
    }

    protected function availableAttributes()
    {
        return [
            'id'        => $this->resource->id,
            'dni'       => $this->dni,
            'email'     => $this->email,
            'name'      => $this->name,
            'last_name' => $this->last_name,
            'address'   => $this->resource->address,
            'commune'   => new CommuneResource($this->whenLoaded('commune')),
            'region'    => new RegionResource($this->whenLoaded('region'))
        ];
    }
}
