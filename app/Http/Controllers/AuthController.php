<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AuthController extends Controller
{

    public function auth(AuthService $authService,Request $request)
    {
        $userAuth = $authService->generateToken($request->only(['email','password']));

        if (!$userAuth) {
            return $this->unauthorized();
        }

        return response()->json([
            'access' => $userAuth->access,
            'user_auth'    => $userAuth->user,
            'token_type'   => 'Bearer'
        ]);

    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    protected function unauthorized()
    {
        return response()->json([ 'message' => 'Unauthorized'],Response::HTTP_UNAUTHORIZED);
    }

}
