<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveCommuneRequest;
use App\Http\Resources\CommuneResource;
use App\Services\CommuneService;

class CommuneController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CommuneService $communeService)
    {
        try {
            $communes = $communeService->index();
            return $this->responseWithResource(CommuneResource::collection($communes),'communes.index');
        } catch (\Exception $e) {
            return $this->responseWithError($e,'communes.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveCommuneRequest $request,CommuneService $communeService)
    {
        try {
            $commune = $communeService->create($request->validated());
            $commune->fresh();
            $commune->load('region');
            return $this->responseWithResource(new CommuneResource($commune),'communes.store');
        } catch (\Exception $e) {
            return $this->responseWithError($e,'communes.store');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(CommuneService $communeService , $commune)
    {
        try {
            $commune = $communeService->show($commune);
            return $this->responseWithResource(new CommuneResource($commune),'commune.show');
        } catch (\Exception $e) {
            return $this->responseWithError($e,'commune.show');
        }
    }
}
