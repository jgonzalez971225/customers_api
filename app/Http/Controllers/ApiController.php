<?php namespace App\Http\Controllers;

class ApiController extends Controller
{

    
    protected function translate($message)
    {
        return __('api.'.$message);
    }

    protected function translateError($message)
    {
        return __('api_errors.'.$message);
    }

    public function responseWithError($exception , $message ,$errors = [], $code = 400)
    {
        return response()->json([
            'exeption' => $exception->getMessage(),
            'errors'   => $errors,
            'message'  => $this->translateError($message)
        ]);
    }

    public function responseWithMessage($message,$status = 200)
    {
        return response()->json($message,$status);
    }

    public function responseWithResource($resource,$message , $status = 200)
    {
        return $resource
            ->additional([
                'message' => $this->translate($message)
            ]);
    }

}