<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveRegionRequest;
use App\Http\Resources\RegionResource;
use App\Services\RegionService;
use Illuminate\Http\Request;

class RegionController extends ApiController
{

    public function index(RegionService $regionService)
    {
        try {
            $regions = $regionService->index();
            return $this->responseWithResource(RegionResource::collection($regions),'region.index');
        } catch (\Exception $e) {
            return $this->responseWithError($e,'regions.index');
        }
    }

    public function store(SaveRegionRequest $request , RegionService $regionService)
    {
        try {
            $region = $regionService->create($request->validated());
            return $this->responseWithResource(new RegionResource($region),'region.store');
        } catch (\Exception $e) {
            return $this->responseWithError($e,'regions.store');
        }
    }

    public function show(RegionService $regionService , $region)
    {
        try {
            $region = $regionService->show($region);
            return $this->responseWithResource(new RegionResource($region),'region.show');
        } catch (\Exception $e) {
            return $this->responseWithError($e,'regions.show');
        }
    }
}
