<?php

namespace App\Http\Controllers;

use App\Http\Requests\SaveCustomerRequest;
use App\Http\Resources\CustomerResource;
use App\Services\CustomerService;

class CustomerController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CustomerService $customerService)
    {
        try {
            $customers = CustomerResource::collection($customerService->index());
            return $this->responseWithResource($customers,'customers.index');
        } catch (\Exception $e) {
            return $this->responseWithError($e,'customers.index');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveCustomerRequest $request , CustomerService $customerService)
    {
        try {
            $customer = $customerService->create($request->validated());
            return $this->responseWithResource(CustomerResource::make($customer),'customers.store',201);
        } catch (\Exception $e) {
            return $this->responseWithError($e,'customers.store');
        }
    }

    public function show(CustomerService $customerService, $customer)
    {
        try {
            $customer = $customerService->show($customer);
            return $this->responseWithResource(CustomerResource::make($customer),'customers.store',200);
        } catch (\Exception $e) {
            return $this->responseWithError($e,'customers.show');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( CustomerService $customerService , $customer)
    {
        try {

            $customerService->handleDestroy($customer);
            return  $this->responseWithMessage('Deleted Customer',204);
        } catch (\Exception $e) {
            return $this->responseWithError($e,'customers.destroy');
        }
    }
}
