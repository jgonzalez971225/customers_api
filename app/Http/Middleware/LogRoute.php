<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class LogRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        if (app()->environment('local'))
        {
            $log = $this->buildLog($request,$response->getContent());
            Log::channel('output_information')->info(json_encode($log));
        }
        return $response;
    }

    protected function buildLog($request,$content)
    {
        return [
            'URL' => $request->getUri(),
            'METHOD' => $request->getMethod(),
            'BODY'   => $request->all(),
            'RESPONSE' => $content
        ];
    }
}
