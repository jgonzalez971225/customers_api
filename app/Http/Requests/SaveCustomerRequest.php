<?php

namespace App\Http\Requests;

use App\Rules\CommuneBelongRegion;
use Illuminate\Foundation\Http\FormRequest;

class SaveCustomerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => ['required'],
            'last_name'   => ['required'],
            'email'       => ['required','unique:customers,email,'.$this->customer],
            'dni'         => ['required','unique:customers,dni,'.$this->customer],
            'address'     => ['required'],
            'status'      => ['nullable'],
            'date_region' => ['nullable'],
            'region_id'   => ['required','integer','exists:regions,id'],
            'commune_id'  => [
                'required',
                'integer',
                'exists:communes,id',
                new CommuneBelongRegion($this->commune_id,$this->region_id)
            ]
        ];
    }
}
