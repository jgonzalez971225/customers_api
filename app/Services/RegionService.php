<?php namespace App\Services;

use App\Models\Region;

class RegionService implements BaseServiceInterface
{
    public function index()
    {
        return Region::with(['communes:id,region_id,description'])
            ->latest()
            ->pattern(request()->pattern)
            ->actives()
            ->paginate();
    }

    public function show($resourceId)
    {
        return Region::with(['communes:id,region_id,description'])
            ->select('id','description')
            ->findOrFail($resourceId);
    }

    public function create(array $validatedData)
    {
        return Region::create($validatedData);
    }

    public function update($resourceId,array $validatedData)
    {

    }

    public function handleDestroy($customerId)
    {
    }

    public function remove($customer)
    {
    }

    public function delete($customer)
    {
    }
}