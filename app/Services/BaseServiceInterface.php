<?php namespace App\Services;

interface BaseServiceInterface
{

    public function index();
    public function show($resourceId);
    public function create(array $data);
    public function update($resourceId,array $validatedData);
    public function handleDestroy($resourceId);
    public function delete($resource);
    public function remove($resource);

}