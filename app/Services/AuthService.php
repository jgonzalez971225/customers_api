<?php namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Carbon;

class AuthService
{

    protected $user;
    public function __construct()
    {
        $this->user = new User();
    }

    protected function authentication(array $credentials)
    {
        $user = $this->user->where('email',$credentials['email'])->first();

        if (!$user){
            return false;
        }

        if (!Hash::check($credentials['password'],$user->password)){
            return false;
        }

        return Auth::loginUsingId($user->id);

    }

    public function generateToken($credentials)
    {
        $userAuth = $this->authentication($credentials);

        if (!$userAuth) {
            return false;
        }

        $tokenResult = $userAuth->createToken('Personal Access Token');
        $token       = $tokenResult->token;

        if (request()->remember_token)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return (object) [
            'access' => [
                'token' =>  $tokenResult->accessToken,
                'expires_at'   => Carbon::parse($token->expires_at)->toDateTimeString()
            ],
            'user' => [
                'email' => $userAuth->email,
                'latest_loggin' => now()->format('d-m-Y h:m:s')
            ]
        ];

    }

}