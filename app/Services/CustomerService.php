<?php namespace App\Services;

use App\Models\Customer;
use App\Services\BaseServiceInterface;

class CustomerService implements BaseServiceInterface
{
    public function index()
    {
        return Customer::with(['commune:id,description,region_id','region:id,description'])
            ->pattern(request()->pattern)
            ->latest()
            ->actives()
            ->paginate();
    }

    public function show($resourceId)
    {
        return Customer::with(['commune:id,description,region_id','region:id,description'])->findOrFail($resourceId);
    }

    public function create(array $validatedData)
    {
        return Customer::create($validatedData);
    }

    public function update($resourceId,array $validatedData)
    {

    }

    public function handleDestroy($customerId)
    {

        $action = request()->action;

        $customer = $this->show($customerId);

        switch ($action) {
            case 'delete':
                $this->remove($customer);
                return $this->delete($customer);
                break;
            case 'remove':
                return $this->remove( $customer);
            default:
                return $this->remove($customer);
                break;
        }

    }

    public function remove($customer)
    {
        return $customer->update(['status' => 'I']);
    }

    public function delete($customer)
    {
        return $customer->delete();
    }
}