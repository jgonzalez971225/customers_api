<?php namespace App\Services;

use App\Models\Commune;

class CommuneService implements BaseServiceInterface
{
    public function index()
    {
        return Commune::with(['region:id,description'])
            ->latest()
            ->pattern(request()->pattern)
            ->actives()
            ->paginate();
    }

    public function show($resourceId)
    {
        return Commune::with(['region:id,description'])
            ->select('id','description','region_id')
            ->findOrFail($resourceId);
    }

    public function create(array $validatedData)
    {
        return Commune::create($validatedData);
    }

    public function update($resourceId,array $validatedData)
    {

    }

    public function handleDestroy($customerId)
    {
    }

    public function remove($customer)
    {
    }

    public function delete($customer)
    {
    }
}