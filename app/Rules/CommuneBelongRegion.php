<?php

namespace App\Rules;

use App\Services\CommuneService;
use Illuminate\Contracts\Validation\Rule;

class CommuneBelongRegion implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($communeId , $regionId)
    {
        $this->communeId = $communeId;
        $this->regionId  = $regionId;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $communeService = new CommuneService();
        $commune        = $communeService->show($this->communeId);
        return $commune->isBelongs($this->regionId);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Commune does not belong to the region';
    }
}
