<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\CommuneController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\RegionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1','middleware'=> ['logroute']],function(){
    Route::post('auth',[AuthController::class,'auth']);
});


Route::group(['prefix' => 'v1','middleware'=>['auth:api','logroute']],function() {

    Route::group(['middleware' => ['prevent.api.key']],function(){

        Route::post('logout',[AuthController::class,'logout']);
        Route::apiResource('regions',RegionController::class)->names('api.v1.regions')->only(['index','store','show']);
        Route::apiResource('communes',CommuneController::class)->names('api.v1.communes')->only(['index','store','show']);
        Route::apiResource('customers',CustomerController::class)->names('api.v1.customers')->except(['update']);

    });

});