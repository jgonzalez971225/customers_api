<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>
# API DOCUMENTATION
<a target="_blank" href="https://documenter.getpostman.com/view/12566856/UVsPNPy5">
    Documentation
</a>


# Run to start with the project

- Composer install
- copy .env.example .env
- php artisan key:generate
- php artisan migrate --seed
- php artisan passport:install

# ADD VARIABLES TO .ENV
ACTIVITY_LOGGER_DB_CONNECTION

# DEFAULT USER CREDENTIALS
- name: user root
- email: admin@admin.com
- password: password-root

# DEFAULT API KEY
use this api key for all request HTTP (this api key is only for test) <br>
?api_key=66a042f0-bc78-4cdb-bb12-45c26b9c6ac7 <br>

<strong>example</strong> <br>
http://localhost://api/v1/customers?pattern=gmail&api_key=66a042f0-bc78-4cdb-bb12-45c26b9c6ac7

# LOGS
* <a target="_blank" href="https://spatie.be/docs/laravel-activitylog/v4/introduction">spatie activity logs</a> (entry)
* Log::channel('ouput') (storage/logs/output.log)
<br> See middleware Middlewares/LogRoute
