<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {

            $table->id();

            $table->unsignedBigInteger('region_id');
            $table->unsignedBigInteger('commune_id');

            $table->string('name',20);
            $table->string('last_name',20);
            $table->string('email')->unique()->index();
            $table->string('dni')->unique()->index();
            $table->longText('address');

            $table->enum('status',['A','I'])->default('A');

            $table->dateTime('date_region')->nullable();

            $table->foreign('region_id')->references('id')->on('regions');
            $table->foreign('commune_id')->references('id')->on('communes');

            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
